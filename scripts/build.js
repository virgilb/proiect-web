const fs = require('fs-extra');
const path = require('path');
const handlebars = require('handlebars');

// PATHS
const PAGES_DIR = path.resolve(__dirname, '..', 'src', 'pages');
const OUTPUT_DIR = path.resolve(__dirname, '..', 'build');
const TEMPLATES_DIR = path.resolve(__dirname, '..', 'src', 'templates');
const COMPONENTS_DIR = path.resolve(__dirname, '..', 'src', 'components');
const ASSETS_DIR = path.resolve(__dirname, '..', 'src', 'assets');
// CONFIG
const DEFAULT_TEMPLATE = 'main';

// ASSETS

const copyAssets = () => {
  fs.copySync(ASSETS_DIR, `${OUTPUT_DIR}/assets`);
}

const prebuildCheck = () => {
  try {
    fs.mkdirSync(OUTPUT_DIR);
  } catch (e) {
    console.log('Build directory check: OK')
  }
}

const registerBasicPartials = () => {
  const headerPath = path.resolve(COMPONENTS_DIR, 'header.html');
  const footerPath = path.resolve(COMPONENTS_DIR, 'footer.html');

  const headerContent = fs.readFileSync(headerPath, 'utf-8');
  const footerContent = fs.readFileSync(footerPath, 'utf-8');
  
  handlebars.registerPartial(
    'header',
    headerContent
  );
  handlebars.registerPartial(
    'footer',
    footerContent
  );
}

const registerContentPartial = (pageContent) => {
  handlebars.registerPartial(
    'content',
    pageContent
  );
}

const preparePageCompile = () => {
  const templatePath = TEMPLATES_DIR + '/' + DEFAULT_TEMPLATE + '.html';
  const templateContent = fs.readFileSync(templatePath, 'utf-8');
  const compile = handlebars.compile(templateContent);

  return compile;
}

const build = () => {
  const makePage = preparePageCompile();
  const pages = fs.readdirSync(PAGES_DIR);
  
  if(pages.length > 0) {
    pages.forEach(page => {
      const pagePath = path.resolve(PAGES_DIR, page);
      const pageContent = fs.readFileSync(pagePath, 'utf-8');

      registerBasicPartials();
      registerContentPartial(pageContent);
      
      const output = makePage();
      
      fs.writeFileSync(`${OUTPUT_DIR}/${page}`, output);
    });
  }
}

prebuildCheck();
copyAssets();
build();